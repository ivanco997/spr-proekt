#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>

typedef struct change
{
	double val;
	struct change *next;
}change_t;

int input_num();
void report();
void del_List(change_t **head);
void print_List(change_t *head);
int stop();
int cash(double price,int flag, change_t **head);
int add_to_end(change_t *head, double t_val);
int add_to_start(change_t **head, double t_val);

double money[2][10]={{0.1,0.2,0.5,1,2,5,10,20,50,100},{0,0,0,0,0,0,0,0,0,0}};

int main()
{
	int flag, status=0;
	int pipefd[2];
	char mn[7];
	double price,price2,price3;
	pid_t cpid;
	change_t *head=NULL;
	price=price2=price3=0;

	if(input_num())
	{
		return 1;
	}
	report();
	while(1)
	{
		if(-1==pipe(pipefd))
		{
			perror("Pipe");
			return 1;
		}

		cpid=fork();
		if(cpid<0)
		{
			printf("Forking has faild!\n");
			return 1;
		}

		if(0==cpid)
		{
			do
			{
				printf("Въведете сума на покупката: ");
				scanf("%s",mn);
				getchar();
				price2=atof(mn);
				memset(mn,0,7);
			}
			while(0>price2||0==price2);
			label: printf("Въведете стойността на предоставените пари: ");
			scanf("%s",mn);
			getchar();
			price3=atof(mn);
			memset(mn,0,7);
			if(price3<price2||0==price3||0>price3)
			{
				printf("Недостатъчни средства\n");
				goto label;
			}
			if(-1==close(pipefd[0]))
			{
				perror("CPID-Close0: ");
				return 1;
			}
			if(-1==write(pipefd[1],&price2,sizeof(price2)))
			{
				perror("(1) Writing: ");
				return 1;
			}
			if(-1==write(pipefd[1],&price3,sizeof(price3)))
			{
				perror("(2) Writing: ");
				return 1;
			}
			if(-1==close(pipefd[1]))
			{
				perror("CPID-Close1: ");
				return 1;
			}
			price2=price3=0;
			exit(status);
		}

		else
		{
			if(-1==wait(&status))
			{
				perror("Wait");
				return 1;
			}
			if(-1==close(pipefd[1]))
			{
				perror("PID-Close1: ");
				return 1;
			}
			if(-1==read(pipefd[0],&price2,sizeof(price2)))
			{
				perror("(1) Reading: ");
				return 1;
			}
			if(-1==read(pipefd[0],&price3,sizeof(price3)))
			{
				perror("(2) Reading: ");
				return 1;
			}
			if(-1==close(pipefd[0]))
			{
				perror("PID-Close0: ");
				return 1;
			}
			price=price3-price2;
			printf("Сума за връщане: %lf\n",price);
			flag=1;

			if(-1==cash(price,flag,&head))
			{
				printf("Грешка при смятане на рестото\n");
				return 1;
			}
			print_List(head);
			del_List(&head);
			if(stop())
			{
				break;
			}
		}
	}
	report();
	return 0;
}

int input_num()
{
	int n;
	char num[7];
	printf("Ваведете броя на парите, с които разполагате: ");
	scanf("%s",num);
	getchar();
	n=atoi(num);
	if(n<0||0==n)
        {
                printf("Невалиден или недостатъчен брой за работа.\n");
                return 1;
        }
        for(int i=0;i<10;i++)
        {
                money[1][i]=n;
        }
	return 0;

}

void report()
{
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<10;j++)
		{
			printf("%lf ",money[i][j]);
		}
	printf("\n");
	}
}

void del_List(change_t **head)
{
	change_t *temp=*head;
	change_t *t_next;
	while(NULL!=temp)
	{
		t_next=temp->next;
		free(temp);
		temp=t_next;
	}
	*head=NULL;
	printf("\n");
}

void print_List(change_t *head)
{
	change_t *temp;
	printf("Ресто: ");
	temp=head;
	while(NULL!=temp)
	{
		printf("%lf ",temp->val);
		temp=temp->next;
	}
}

int stop()
{
	char stop;
	printf("Искате ли да излезете?\nЗа излизане натиснете Q, за продължаване - Enter: ");
	stop=getchar();
	if('q'==stop||'Q'==stop)
	{
		printf("Вие излязохте.\n");
		return 1;
	}
	else
	{
		while('\n'!=stop)
		{
			stop=getchar();
		}
		return 0;
	}
}

int cash(double price, int flag, change_t **head)
{
	for(int i=9;i>=0;i--)
	{
		while(price>=money[0][i])
		{
			if(0!=i)
			{
				while(0==money[1][i])
				{
					i--;
				}
			}
			price-=money[0][i];
			money[1][i]-=1;

			if(flag)
			{
				flag=add_to_start(head,money[0][i]);
				if(-1==flag)
				{
					printf("Грешка при добавянето в началото\n");
					return -1;
				}
			}
			else
			{
				if(-1==add_to_end((*head),money[0][i]))
				{
					printf("Грешка при добавянето в края\n");
					return -1;
				}
			}
		}
	}
	return 0;
}

int  add_to_end(change_t *head, double t_val)
{
	change_t *tmp=head;
	while(tmp->next!=NULL)
	{
		tmp=tmp->next;
	}
	tmp->next=malloc(sizeof(change_t));
	if(NULL==tmp->next)
	{
		printf("Незаделяне на памет 2\n");
		return -1;
	}
	tmp->next->val=t_val;
	tmp->next->next=NULL;
}

int add_to_start(change_t **head, double t_val)
{
	(*head)=malloc(sizeof(change_t));
	if(NULL==head)
	{
		printf("Незаделяне на памет\n");
		return -1;
	}
	(*head)->val=t_val;
	(*head)->next=NULL;
	return 0;
}
